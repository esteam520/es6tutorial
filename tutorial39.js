/*
   Iterating Objects
*/


/* Sample 1: By default
   Output:
        TypeError: person[Symbol.iterator] is not a function
*/
/*
let person = {
  	fname: "Chandler",
  	lname: "Bing"
};


for (let p of person){
  	console.log(p);

}
*/


/* Sample 2: To define a person function with Symbol.iterator key
   Output:
        Chandler
        Bing
*/
let person = {
  	fname: "Chandler",
  	lname: "Bing"
};

person[Symbol.iterator] = function(){
let properties = Object.keys(person);
let count = 0;
let isDone = false;
let next = () => {
		if(count>=properties.length){
      	isDone = true;
    }
  	return{done: isDone, value: this[properties[count++]]};
}
return {next};

}

for (let p of person){
  	console.log(p);

}

