"use strict";
/* Sample 1: Basic idea
   Output:
        Chandler
        Bing
        Male
*/
/* let employee = ["Chandler", "Bing", "Male"];

let [fname, lname, gender] = employee;

console.log(fname);
console.log(lname);
console.log(gender); */
/* Sample 2
   Output:
        Chandler
        Bing
        undefined
*/
/* let employee = ["Chandler", "Bing"];

let [fname, lname, gender] = employee;

console.log(fname);
console.log(lname);
console.log(gender); */
/* Sample 3
   Output:
        Male
*/
/* let employee = ["Chandler", "Bing", "Male"];

let [, , gender] = employee;

//console.log(fname);
//console.log(lname);
console.log(gender); */
/* Sample 4: Work with Rest Operator
   Output:
        Chandler
        ["Bing", "Male"]
*/
/* let employee = ["Chandler", "Bing", "Male"];

let [fname, ...elements] = employee;

console.log(fname);
console.log(elements);
//console.log(gender); */
/* Sample 5: Destructuring with default value
   Output:
        Chandler
        Bing
        Male
*/
/* let employee = ["Chandler", "Bing"];

let [fname, lname, gender="Male"] = employee;

console.log(fname);
console.log(lname);
console.log(gender); */
/* Sample 6: Destructuring with default value
   Output:
        Chandler
        Bing
        Female
*/
var employee = ["Chandler", "Bing", "Female"];
var fname = employee[0], lname = employee[1], _a = employee[2], gender = _a === void 0 ? "Male" : _a;
console.log(fname);
console.log(lname);
console.log(gender);
//# sourceMappingURL=tutorial17.js.map