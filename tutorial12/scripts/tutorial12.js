"use strict";
/* Sample 1
   Output:
        undefined
*/
/* let getValue = function(value){
    console.log(value);
};
getValue(); */
/* Sample 2
   Output:
        10
        20
*/
/* let getValue = function(value=10){
    console.log(value);
};
getValue();
getValue(20); */
/* Sample 3
   Output:
        15
        25
        50
        40
*/
/* let getValue = function(value=10, bonus=5){
    console.log(value+bonus);
};
getValue();
getValue(20);
getValue(20,30);
getValue(undefined,30); */
/* Sample 4
   Output:
        11
        22
        50
        40
*/
/* let getValue = function(value=10, bonus=value*0.1){
    console.log(value+bonus);
};
getValue();
getValue(20);
getValue(20,30);
getValue(undefined,30); */
/* Sample 5
   Output:
        11
        22
        50
        40
*/
/* let percentBonus = 0.1;
let getValue = function(value=10, bonus=value*percentBonus){
    console.log(value+bonus);
};
getValue();
getValue(20);
getValue(20,30);
getValue(undefined,30); */
/* Sample 6
   Output:
        11
        22
        50
        40
*/
/* let percentBonus = () => 0.1;
let getValue = function(value=10, bonus=value*percentBonus()){
    console.log(value+bonus);
};
getValue();
getValue(20);
getValue(20,30);
getValue(undefined,30); */
/* Sample 7
   Output:
        11
        0
        22
        1
        50
        2
        40
        2
*/
var percentBonus = function () { return 0.1; };
var getValue = function (value, bonus) {
    if (value === void 0) { value = 10; }
    if (bonus === void 0) { bonus = value * percentBonus(); }
    console.log(value + bonus);
    console.log(arguments.length);
};
getValue();
getValue(20);
getValue(20, 30);
getValue(undefined, 30);
//# sourceMappingURL=tutorial12.js.map