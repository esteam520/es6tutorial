/*
   Tutorial 28: Exporting Functions and Classes
*/


/* Sameple 1: import function
   Output:
        Hello World
*/
/* import {greet} from './moduleB.js'

greet("Hello World"); */


/* Sameple 2: import class
   Output:
        Hello World
        Constructor
        Greet function
*/
import {greet, GreetMessage} from './moduleB.js'

greet("Hello World");

let gm = new GreetMessage();
gm.greet();