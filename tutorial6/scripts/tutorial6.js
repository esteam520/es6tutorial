"use strict";
function greetPerson(name) {
    var greet;
    if (name === "Chandler") {
        greet = "Hello Chandler";
    }
    else {
        greet = "Hi there";
    }
    console.log(greet);
}
greetPerson("Chandler");
var a = 1;
var b = 2;
if (a === 1) {
    var a = 10;
    var b_1 = 20;
    console.log(a); //10
    console.log(b_1); //20
}
console.log(a); //10
console.log(b); //2
var c = 1;
var c = 2;
// let keword can't be re-declared in the scope
var d = 1;
//let d=2;
//# sourceMappingURL=tutorial6.js.map