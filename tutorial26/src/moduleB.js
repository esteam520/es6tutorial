/* Sameple 1 */
//export let fname = "Chandler";


/* Sameple 2 */
//export let fname = "Chandler";
//export let lname = "Bing";


/* Sameple 3, Sameple 4 */
/* let fname = "Chandler";
let lname = "Bing";

export {fname, lname} */


/* Sameple 5, Sameple 6 */
/* let fname = "Chandler";
let lname = "Bing";
console.log(`Module B loaded`);
export {fname, lname} */


/* Sameple 7 */
let fname = "Chandler";
let lname = "Bing";

let obj = {
    name: "Joey"
};
console.log(`Module B loaded`);
export {fname, lname, obj}