/*
   Tutorial 26: Named Exports
*/


/* Sameple 1
   Output:
        Chandler
*/
/* import {fname} from './moduleB.js'

console.log(fname); */


/* Sameple 2, Sameple 3
   Output:
        Chandler Bing
*/
/* import {fname, lname} from './moduleB.js'

console.log(` ${fname} ${lname}`); */


/* Sameple 4: Use alias
   Output:
        Chandler Bing
*/
/* import {fname as f, lname as l} from './moduleB.js'

console.log(` ${f} ${l}`); */


/* Sameple 5: Whenever there is import, it will be hoisted.
   Output:
        Module B loaded
        Module A log 1
        Module A log 2
         Chandler Bing
*/
/* console.log('Module A log 1');
import {fname as f, lname as l} from './moduleB.js'  // log Module B loaded
console.log('Module A log 2');
console.log(` ${f} ${l}`); */


/* Sameple 6: Import are read only.
   Output:
        Get error - The error means you can't change something that is imported.
*/
/* import {fname, lname} from './moduleB.js'  // log Module B loaded

fname = "Ross";

console.log(` ${fname} ${lname}`); */


/* Sameple 7: What you can change is the properties of objects.
   Output:
        Module B loaded
        Ross
         Chandler Bing
*/
import {fname, lname, obj} from './moduleB.js'  // log Module B loaded

obj.name = "Ross";
console.log(obj.name);
console.log(` ${fname} ${lname}`);