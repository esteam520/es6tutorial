"use strict";
/*
   WeakSets
*/
/* Sample 1: Set
   Output:
        1
*/
/* let mySet = new Set();
let key = {};
mySet.add(key);
console.log(mySet.size); */
/* Sample 2: Set
   Output:
        1
        1
*/
/* let mySet = new Set();
let key = {};
mySet.add(key);
console.log(mySet.size);
key = null;
console.log(mySet.size);
key = [...mySet][0]; */
/* Sample 3: WeakSet
   Output:
        true
*/
var set = new WeakSet();
var key = {};
set.add(key);
console.log(set.has(key));
key = null;
//# sourceMappingURL=tutorial31.js.map