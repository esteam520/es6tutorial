/*
   Iterating over Maps
*/


/* Sample 1: Retrieve just keys
   Output:
        fname
        lname
*/
/* let myMap = new Map([
	["fname", "Chandler"],
  ["lname", "Bing"]
]);

for(let key of myMap.keys()){
  console.log(key);
} */


/* Sample 2: Retrieve just values
   Output:
   			Chandler
        Bing
        fname
        lname
*/
/* let myMap = new Map([
	["fname", "Chandler"],
  ["lname", "Bing"]
]);

for(let value of myMap.values()){
  console.log(value);
}

for(let key of myMap.keys()){
  console.log(key);
} */


/* Sample 3: Retrieve bother keys and values - by using entry
   Output:
   			fname -> Chandler
        lname -> Bing
   			Chandler
        Bing
        fname
        lname
*/
/* let myMap = new Map([
	["fname", "Chandler"],
  ["lname", "Bing"]
]);

for(let entry of myMap.entries()){
  console.log(`${entry[0]} -> ${entry[1]}`);
}

for(let value of myMap.values()){
  console.log(value);
}

for(let key of myMap.keys()){
  console.log(key);
} */


/* Sample 4: Retrieve both keys and values - by using key - value pairs
   Output:
   			fname -> Chandler
        lname -> Bing
   			Chandler
        Bing
        fname
        lname
*/
let myMap = new Map([
	["fname", "Chandler"],
  ["lname", "Bing"]
]);

for(let [key,value] of myMap.entries()){
  console.log(`${key} -> ${value}`);
}

for(let value of myMap.values()){
  console.log(value);
}

for(let key of myMap.keys()){
  console.log(key);
}