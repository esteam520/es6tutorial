/*
   Class
*/

/* Sample 1
   Output:
        function
*/
/* class Person{}

let p = new Person();

console.log(typeof Person); */


/* Sample 2
   Output:
        function
*/
/* class Person{}

let p = new Person();

employee();
function employee(){}
employee();

console.log(typeof Person); */


/* Sample 3: Classes are not hoisted.
   Output:
        tutorial21.ts:35 Uncaught TypeError: Person is not a constructor
*/
/* let p1 = new Person();
class Person{}
let p = new Person();

employee();
function employee(){}
employee();

console.log(typeof Person); */


/* Sample 4
   Output:
        true
*/
class Person{
    greet(){}
}
let p = new Person();

console.log(p.greet === Person.prototype.greet);
