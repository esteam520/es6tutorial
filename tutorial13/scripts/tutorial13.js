"use strict";
/* Sample 1
   Output:
        Red
        Red
        Blue
        Red
        Blue
        Green
*/
/* let displayColors = function(){
    for(let i in arguments){
        console.log(arguments[i]);

    }
}


displayColors('Red');
displayColors('Red','Blue');
displayColors('Red','Blue','Green'); */
/* Sample 2
   Output:
        List of colors
        List of colors
        Red
        List of colors
        List of colors
        Red
        Blue
        List of colors
        List of colors
        Red
        Blue
        Green
*/
/* let displayColors = function(){

    console.log(message);

    for(let i in arguments){
        console.log(arguments[i]);

    }
}

let message = "List of colors"

displayColors(message, 'Red');
displayColors(message, 'Red','Blue');
displayColors(message, 'Red','Blue','Green');


displayColors('Red');
displayColors('Red','Blue');
displayColors('Red','Blue','Green'); */
/* Sample 3
   Output:
        List of colors
        Red
        List of colors
        Red
        Blue
        List of colors
        Red
        Blue
        Green
*/
/* let displayColors = function(message, ...colors){

    console.log(message);

    for(let i in colors){
        console.log(colors[i]);

    }
}

let message = "List of colors"

displayColors(message, 'Red');
displayColors(message, 'Red','Blue');
displayColors(message, 'Red','Blue','Green'); */
/* Sample 4
   Output:
        List of colors
        ["Red"]
        Red
        List of colors
        ["Red", "Blue"]
        Red
        Blue
        List of colors
        ["Red", "Blue", "Green"]
        Red
        Blue
        Green
*/
/* let displayColors = function(message, ...colors){

    console.log(message);
    console.log(colors);

    for(let i in colors){
        console.log(colors[i]);

    }
}

let message = "List of colors"

displayColors(message, 'Red');
displayColors(message, 'Red','Blue');
displayColors(message, 'Red','Blue','Green'); */
/* Sample 5
   Output:
        List of colors
        ["Red"]
        2
        Red
        List of colors
        ["Red", "Blue"]
        3
        Red
        Blue
        List of colors
        ["Red", "Blue", "Green"]
        4
        Red
        Blue
        Green
*/
var displayColors = function (message) {
    var colors = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        colors[_i - 1] = arguments[_i];
    }
    console.log(message);
    console.log(colors);
    console.log(arguments.length);
    for (var i in colors) {
        console.log(colors[i]);
    }
};
var message = "List of colors";
displayColors(message, 'Red');
displayColors(message, 'Red', 'Blue');
displayColors(message, 'Red', 'Blue', 'Green');
//# sourceMappingURL=tutorial13.js.map