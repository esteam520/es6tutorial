/*
   forEach
*/


/* Sample 1: forEach loop with Arrays
   Output:
        arr[0]=2
        arr[1]=4
        arr[2]=6
        arr[3]=8
*/
/*
let numbers = [2,4,6,8];
numbers.forEach(arrayFunction);
function arrayFunction(element,index,array){
  console.log("arr["+index+"]="+element);
};
*/



/* Sample 2: forEach loop with Maps
   Output:
        fname Chandler
        true
        lname Bing
        true
*/
/*
let myMap = new Map([["fname","Chandler"],
                     ["lname","Bing"]]);
myMap.forEach(mapFunction);
function mapFunction(value,key,callingMap){
  console.log(key+" "+value);
  console.log(myMap === callingMap);
}
*/



/* Sample 3: forEach loop with Sets
   Output:
        1 1
        true
        2 2
        true
        3 3
        true
*/
let mySet = new Set([1,2,3]);
mySet.forEach(setFunction);
function setFunction(value,key,callingSet){
  console.log(key+" "+value);
  console.log(mySet === callingSet);
}

