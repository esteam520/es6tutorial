/*
   Enumeration
*/


/* Sample 1
    Result:
        5
*/
/*
enum EyeColor {Brown=1, Black=5, Blue=10};

var myEyeColor = EyeColor.Black;

console.log(myEyeColor);
*/


/* Sample 2
    Result:
        5
        Black
*/
enum EyeColor {Brown=1, Black=5, Blue=10};

var myEyeColor = EyeColor.Black;

console.log(myEyeColor);
console.log(EyeColor[myEyeColor]);
