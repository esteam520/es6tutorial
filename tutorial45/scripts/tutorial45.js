"use strict";
/*
   Enumeration
*/
/* Sample 1
    Result:
        5
*/
/*
enum EyeColor {Brown=1, Black=5, Blue=10};

var myEyeColor = EyeColor.Black;

console.log(myEyeColor);
*/
/* Sample 2
    Result:
        5
        Black
*/
var EyeColor;
(function (EyeColor) {
    EyeColor[EyeColor["Brown"] = 1] = "Brown";
    EyeColor[EyeColor["Black"] = 5] = "Black";
    EyeColor[EyeColor["Blue"] = 10] = "Blue";
})(EyeColor || (EyeColor = {}));
;
var myEyeColor = EyeColor.Black;
console.log(myEyeColor);
console.log(EyeColor[myEyeColor]);
//# sourceMappingURL=tutorial45.js.map