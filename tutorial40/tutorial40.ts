/*
   Generators
*/


/* Sample 1
   Output:
        {value: 1, done: false}
*/
/*
function * createGenerator(){
    yield 1;
    console.log("After 1st yield");
    yield 2;
}

let myGen = createGenerator();

console.log(myGen.next());
*/


/* Sample 2
   Output:
        {value: 1, done: false}
        After 1st yield
        {value: 2, done: false}
        {value: undefined, done: true}
*/
/*
function * createGenerator(){
	yield 1;
    console.log("After 1st yield");
    yield 2;
}

let myGen = createGenerator();

console.log(myGen.next());
console.log(myGen.next());
console.log(myGen.next());
*/


/* Sample 3: Rewrite the last course sample by using Generator
   Output:
        Chandler
        Bing
*/
//Replace the following codes with Gererator codes
let person = {
    fname: "Chandler",
    lname: "Bing"
};

person[Symbol.iterator] = function*(){
let properties = Object.keys(person);
for(let t of properties){
yield this[t];
}
};
for (let p of person){
    console.log(p);
}

