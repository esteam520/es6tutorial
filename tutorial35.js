/*
   WeakMaps (Seldom to use just to show it's do exists)
*/


/* Sample 1
   Output:
        Hello World
*/
/*
let myMap = new WeakMap();
let obj1 = {};
myMap.set(obj1,"Hello World");
console.log(myMap.get(obj1));
*/


/* Sample 2
   Output:
        "ReferenceError: obj2 is not defined
*/
let myMap = new WeakMap();
let obj1 = {};
myMap.set(obj1,"Hello World");
console.log(myMap.get(obj1));
obj2 = null;