"use strict";
/*
   Class Inheritance
*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/* Sample 1
   Output:
        Person constructor
*/
/* class Person{
    constructor(){
        console.log("Person constructor");

    }
}

class Employee extends Person{

}

let e = new Employee(); */
/* Sample 2
   Output:
        Chandler Person constructor
*/
/* class Person{
    constructor(name){
        console.log(name + " Person constructor");

    }
}

class Employee extends Person{

}

let e = new Employee("Chandler"); */
/* Sample 3
   Output:
        Chandler Person constructor
        Chandler Employee constructor
*/
/* class Person{
    constructor(name){
        console.log(name + " Person constructor");

    }
}

class Employee extends Person{
    constructor(name){
        super(name);
        console.log(name + " Employee constructor");

    }
}

let e = new Employee("Chandler"); */
/* Sample 4
   Output:
        Chandler Person constructor
        Chandler Employee constructor
        10
*/
/* class Person{
    constructor(name){
        console.log(name + " Person constructor");

    }
    getID(){
        return 10;
    }
}

class Employee extends Person{
    constructor(name){
        super(name);
        console.log(name + " Employee constructor");

    }
}

let e = new Employee("Chandler");
console.log(e.getID()); */
/* Sample 5
   Output:
        Chandler Person constructor
        Chandler Employee constructor
        //50
        10
*/
var Person = /** @class */ (function () {
    function Person(name) {
        console.log(name + " Person constructor");
    }
    Person.prototype.getID = function () {
        return 10;
    };
    return Person;
}());
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(name) {
        var _this = _super.call(this, name) || this;
        console.log(name + " Employee constructor");
        return _this;
    }
    Employee.prototype.getID = function () {
        //return 50;
        return _super.prototype.getID.call(this);
    };
    return Employee;
}(Person));
var e = new Employee("Chandler");
console.log(e.getID());
//# sourceMappingURL=tutorial23.js.map