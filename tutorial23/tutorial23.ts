/*
   Class Inheritance
*/


/* Sample 1
   Output:
        Person constructor
*/
/* class Person{
    constructor(){
        console.log("Person constructor");

    }
}

class Employee extends Person{

}

let e = new Employee(); */


/* Sample 2
   Output:
        Chandler Person constructor
*/
/* class Person{
    constructor(name){
        console.log(name + " Person constructor");

    }
}

class Employee extends Person{

}

let e = new Employee("Chandler"); */


/* Sample 3
   Output:
        Chandler Person constructor
        Chandler Employee constructor
*/
/* class Person{
    constructor(name){
        console.log(name + " Person constructor");

    }
}

class Employee extends Person{
    constructor(name){
        super(name);
        console.log(name + " Employee constructor");

    }
}

let e = new Employee("Chandler"); */


/* Sample 4
   Output:
        Chandler Person constructor
        Chandler Employee constructor
        10
*/
/* class Person{
    constructor(name){
        console.log(name + " Person constructor");

    }
    getID(){
        return 10;
    }
}

class Employee extends Person{
    constructor(name){
        super(name);
        console.log(name + " Employee constructor");

    }
}

let e = new Employee("Chandler");
console.log(e.getID()); */


/* Sample 5
   Output:
        Chandler Person constructor
        Chandler Employee constructor
        //50
        10
*/
class Person{
    constructor(name){
        console.log(name + " Person constructor");

    }
    getID(){
        return 10;
    }
}

class Employee extends Person{
    constructor(name){
        super(name);
        console.log(name + " Employee constructor");

    }
    getID(){
        //return 50;
        return super.getID();
    }
}

let e = new Employee("Chandler");
console.log(e.getID());
