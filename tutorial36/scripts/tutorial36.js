"use strict";
/*
   Symbols
*/
var _a;
/* Sample 1
   Output:
        symbol
*/
/*
let s = Symbol();
console.log(typeof s);
*/
/* Sample 2
   Output:
        symbol
        Symbol(First Symbol)
*/
/*
let s = Symbol("First Symbol");
console.log(typeof s);
console.log(s.toString());
*/
/* Sample 3: A symbol alays create a unique Id.
   Output:
        symbol
        Symbol(First Symbol)
        false
*/
/*
let s = Symbol("First Symbol");
console.log(typeof s);
console.log(s.toString());

let s2 = Symbol();
let s3 = Symbol();

console.log(s2===3);
*/
/* Sample 4: It donesn't matter what the description is...
   Output:
        symbol
        Symbol(First Symbol)
        false
*/
/*
let s = Symbol("First Symbol");
console.log(typeof s);
console.log(s.toString());

let s2 = Symbol("Test");
let s3 = Symbol("Test");

console.log(s2===3);
*/
/* Sample 5: Built in Symbol registry
   Output:
        symbol
        Symbol(First Symbol)
        false
        true
*/
/*
let s = Symbol("First Symbol");
console.log(typeof s);
console.log(s.toString());

let s2 = Symbol("Test");
let s3 = Symbol("Test");

console.log(s2===3);

let s4 = Symbol.for("RegSymbol");
let s5 = Symbol.for("RegSymbol");

console.log(s4===s5);
*/
/* Sample 6: Access the key that was associated with the Symbol when the Symbol was added to the global registry.
   Output:
        symbol
        Symbol(First Symbol)
        false
        true
        RegSymbol
*/
/*
let s = Symbol("First Symbol");
console.log(typeof s);
console.log(s.toString());

let s2 = Symbol("Test");
let s3 = Symbol("Test");

console.log(s2===3);

let s4 = Symbol.for("RegSymbol");
let s5 = Symbol.for("RegSymbol");

console.log(s4===s5);
console.log(Symbol.keyFor(s4));
*/
/* Sample 7: A good place to use Symbols is in object properties.
   Output:
        symbol
        Symbol(First Symbol)
        false
        true
        RegSymbol
        []
        [Symbol()]
*/
/*
let s = Symbol("First Symbol");
console.log(typeof s);
console.log(s.toString());

let s2 = Symbol("Test");
let s3 = Symbol("Test");

console.log(s2===3);

let s4 = Symbol.for("RegSymbol");
let s5 = Symbol.for("RegSymbol");

console.log(s4===s5);
console.log(Symbol.keyFor(s4));

let fname = Symbol();
let person = {
    [fname]: "Chandler";
};

console.log(Object.getOwnPropertyNames(person));
console.log(Object.getOwnPropertySymbols(person));
*/
/* Sample 8: A good place to use Symbols is in object properties - one more sample.
   Output:
        symbol
        Symbol(First Symbol)
        false
        true
        RegSymbol
        []
        [Symbol(FirstName)]
*/
var s = Symbol("First Symbol");
console.log(typeof s);
console.log(s.toString());
var s2 = Symbol("Test");
var s3 = Symbol("Test");
console.log(s2 === 3);
var s4 = Symbol.for("RegSymbol");
var s5 = Symbol.for("RegSymbol");
console.log(s4 === s5);
console.log(Symbol.keyFor(s4));
var fname = Symbol("FirstName");
var person = (_a = {},
    _a[fname] = "Chandler",
    _a);
console.log(Object.getOwnPropertyNames(person));
console.log(Object.getOwnPropertySymbols(person));
//# sourceMappingURL=tutorial36.js.map