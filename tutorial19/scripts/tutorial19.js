"use strict";
/*
   String Templates
*/
/* Sample 1: Basic
   Output:
        Welcome Chandler to ES2015
*/
/* let user = "Chandler";

let greet = "Welcome " + user + " to ES2015";

console.log(greet); */
/* Sample 2: Working with backticks `
   Output:
        Welcome Chandler to ES2015
*/
/* let user = "Chandler";

let greet = `Welcome ${user} to ES2015`;

console.log(greet); */
/* Sample 3: Use '' and ""
   Output:
        Welcome 'Single' "double" Chandler to ES2015
*/
/* let user = "Chandler";

let greet = `Welcome 'Single' "double" ${user} to ES2015`;

console.log(greet); */
/* Sample 3: Multiple lines working with backticks `
   Output:
        Welcome 'Single' "double" Chandler to ES2015
            This is the second lines
               Third and so          on.
*/
var user = "Chandler";
var greet = "Welcome 'Single' \"double\" " + user + " to ES2015\n            This is the second lines\n               Third and so          on.\n\n";
console.log(greet);
//# sourceMappingURL=tutorial19.js.map