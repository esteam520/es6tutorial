/*
   String Templates
*/

/* Sample 1: Basic
   Output:
        Welcome Chandler to ES2015
*/
/* let user = "Chandler";

let greet = "Welcome " + user + " to ES2015";

console.log(greet); */


/* Sample 2: Working with backticks `
   Output:
        Welcome Chandler to ES2015
*/
/* let user = "Chandler";

let greet = `Welcome ${user} to ES2015`;

console.log(greet); */


/* Sample 3: Use '' and ""
   Output:
        Welcome 'Single' "double" Chandler to ES2015
*/
/* let user = "Chandler";

let greet = `Welcome 'Single' "double" ${user} to ES2015`;

console.log(greet); */


/* Sample 3: Multiple lines working with backticks `
   Output:
        Welcome 'Single' "double" Chandler to ES2015
            This is the second lines
               Third and so          on.
*/
let user = "Chandler";

let greet = `Welcome 'Single' "double" ${user} to ES2015
            This is the second lines
               Third and so          on.

`;

console.log(greet);
