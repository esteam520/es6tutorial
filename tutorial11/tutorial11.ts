/* Sample 1
   Output: 1
*/
var employee = {
    id: 1,
    greet: function(){
        console.log(this.id);
    }
};
employee.greet();


/* Sample 2
   Output: undefined
*/
var employee = {
    id: 1,
    greet: function(){
        setTimeout(function(){console.log(this.id)}, 1000);
    }
};
employee.greet();


/* Sample 3
   Output: 1
*/
var employee = {
    id: 1,
    greet: function(){
        var self = this;
        setTimeout(function(){console.log(self.id)}, 1000);
    }
};
employee.greet();


/* Sample 4: Using Arrow function
   Output: 1
*/
var employee = {
    id: 1,
    greet: function(){

        setTimeout(() => {console.log(this.id)}, 1000);
    }
};
employee.greet();