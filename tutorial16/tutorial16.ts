/* Sample 1: Use namespace inside object property
   Output:
        11Chandler
*/
/* let person = {
    "first name": "Chandler"
};
console.log(person["first name"]); */


/* Sample 2: Use variable as propery names
   Output:
        Object {first name: "Chandler", last name: "Bing"}
*/
let ln = "last name";
let person = {
    "first name": "Chandler",
    [ln]: "Bing"
};
console.log(person);


