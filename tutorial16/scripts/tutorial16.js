"use strict";
/* Sample 1: Use namespace inside object property
   Output:
        11Chandler
*/
/* let person = {
    "first name": "Chandler"
};
console.log(person["first name"]); */
var _a;
/* Sample 2: Use variable as propery names
   Output:
        Object {first name: "Chandler", last name: "Bing"}
*/
var ln = "last name";
var person = (_a = {
        "first name": "Chandler"
    },
    _a[ln] = "Bing",
    _a);
console.log(person);
//# sourceMappingURL=tutorial16.js.map