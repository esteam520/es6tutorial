"use strict";
/* Sample 1
   Output:
        Chandler
        Bing
*/
/* let firstname = "Chandler";
let lastname = "Bing";

let person = {
    firstname: firstname,
    lastname: lastname
};

console.log(person.firstname);
console.log(person.lastname); */
/* Sample 2: Shorten Literals in ES2015
   Output:
        Chandler
        Bing
*/
/* let firstname = "Chandler";
let lastname = "Bings";

let person = {
    firstname,
    lastname
};

console.log(person.firstname);
console.log(person.lastname); */
/* Sample 3: Working with function
   Output:
        Ross
        Geller
        Ross Geller
*/
/* let firstname = "Chandler";
let lastname = "Bings";

let person = {
    firstname,
    lastname
};

function createPerson(firstname, lastname){
    let fullname = firstname + " " + lastname;
    return {firstname, lastname, fullname}
}

let p = createPerson("Ross","Geller");
console.log(p.firstname);
console.log(p.lastname);
console.log(p.fullname); */
//console.log(person.firstname);
//console.log(person.lastname);
/* Sample 4: Working with function embedded function
   Output:
        Ross
        Geller
        Ross Geller
        false
*/
/* let firstname = "Chandler";
let lastname = "Bings";

let person = {
    firstname,
    lastname
};

function createPerson(firstname, lastname, age){
    let fullname = firstname + " " + lastname;
    return {firstname,
        lastname,
        fullname,
        isSenior:function(){
            return age>60;
        }}
}

let p = createPerson("Ross","Geller", 32);
console.log(p.firstname);
console.log(p.lastname);
console.log(p.fullname);
console.log(p.isSenior());


//console.log(person.firstname);
//console.log(person.lastname); */
/* Sample 5: Working with function embedded function: Shorten Literals in ES2015
   Output:
        Ross
        Geller
        Ross Geller
        true
*/
var firstname = "Chandler";
var lastname = "Bings";
var person = {
    firstname: firstname,
    lastname: lastname
};
function createPerson(firstname, lastname, age) {
    var fullname = firstname + " " + lastname;
    return { firstname: firstname,
        lastname: lastname,
        fullname: fullname,
        isSenior: function () {
            return age > 60;
        } };
}
var p = createPerson("Ross", "Geller", 62);
console.log(p.firstname);
console.log(p.lastname);
console.log(p.fullname);
console.log(p.isSenior());
//console.log(person.firstname);
//console.log(person.lastname);
//# sourceMappingURL=tutorial15.js.map