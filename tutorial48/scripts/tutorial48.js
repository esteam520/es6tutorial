"use strict";
/*
   Interfaces
        The only purpose of using Interface in typescript is to describe a particular type.
*/
;
var employee1 = {
    fname: "Chandler",
    lname: "Bing",
    age: 30
};
var employee2 = {
    fname: "Chandler",
    lname: "Bing",
};
//# sourceMappingURL=tutorial48.js.map