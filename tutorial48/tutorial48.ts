/*
   Interfaces
        The only purpose of using Interface in typescript is to describe a particular type.
*/


/* Sample
        When you want to make a property optional, we just have to add this question mark (?) next to the particular property.
*/
interface Person{
    fname: string;
    lname: string;
    age?: number;
};

let employee1: Person = {
    fname:"Chandler",
    lname:"Bing",
    age:30
}

let employee2: Person = {
    fname:"Chandler",
    lname:"Bing",
}