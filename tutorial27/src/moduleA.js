/*
   Tutorial 27: Default Exports
*/


/* Sameple 1: Variable name doesn't required to be match.
   Output:
        Chandler
*/
/* import firstName from './moduleB.js'

console.log(firstName); */


/* Sameple 1: Can provide alias for import default, only thing is that you need to use {}.
   Output:
        Chandler
*/
import {default as f } from './moduleB.js'

console.log(f);