"use strict";
/*
   Maps
*/
/* Sample 1: Map
   Output:
        Chandler
*/
/* let myMap = new Map();

myMap.set("fname","Chandler");
myMap.set("age", 30);

console.log(myMap.get("fname")); */
/* Sample 2: Map - Object as key.
   Output:
        Chandler
        10
*/
/* let myMap = new Map();

myMap.set("fname","Chandler");
myMap.set("age", 30);

console.log(myMap.get("fname"));

let ob1 = {};
let ob2 = {};

myMap.set(ob1, 10);
myMap.set(ob2, 20);

console.log(myMap.get(ob1)); */
/* Sample 3: Map size.
   Output:
        Chandler
        10
        4
*/
/* let myMap = new Map();

myMap.set("fname","Chandler");
myMap.set("age", 30);

console.log(myMap.get("fname"));

let ob1 = {};
let ob2 = {};

myMap.set(ob1, 10);
myMap.set(ob2, 20);

console.log(myMap.get(ob1));
console.log(myMap.size); */
/* Sample 4: Use the has method to check if a key exists in a map.
   Output:
        Chandler
        10
        4
        true
*/
/* let myMap = new Map();

myMap.set("fname","Chandler");
myMap.set("age", 30);

console.log(myMap.get("fname"));

let ob1 = {};
let ob2 = {};

myMap.set(ob1, 10);
myMap.set(ob2, 20);

console.log(myMap.get(ob1));
console.log(myMap.size);
console.log(myMap.has("fname")); */
/* Sample 5: Use the delete method to remove key-value pair.
   Output:
        Chandler
        10
        3
        false
*/
/* let myMap = new Map();

myMap.set("fname","Chandler");
myMap.set("age", 30);

console.log(myMap.get("fname"));

let ob1 = {};
let ob2 = {};

myMap.set(ob1, 10);
myMap.set(ob2, 20);

console.log(myMap.get(ob1));
myMap.delete("fname");

console.log(myMap.size);
console.log(myMap.has("fname")); */
/* Sample 6: Finally, we have a clear method to remove all the key-value pairs from a map.
   Output:
        Chandler
        10
        0
        false
*/
var myMap = new Map();
myMap.set("fname", "Chandler");
myMap.set("age", 30);
console.log(myMap.get("fname"));
var ob1 = {};
var ob2 = {};
myMap.set(ob1, 10);
myMap.set(ob2, 20);
console.log(myMap.get(ob1));
myMap.delete("fname");
myMap.clear();
console.log(myMap.size);
console.log(myMap.has("fname"));
//# sourceMappingURL=tutorial32.js.map