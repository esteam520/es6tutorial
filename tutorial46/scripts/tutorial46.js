"use strict";
/*
   Arrays and Tuples
*/
/* Sample 1: There are 2 ways to declare Arrays
*/
/*
let strArr1: string[] = ['Hello','world'];
let strArr2: Array<string> = ['Hello', 'world'];

let anyArr: any[] = ['Hello', 10, true];
*/

/* Sample 2: Tuples
    Result:
        Hi
        10
*/
/*
let strArr1: string[] = ['Hello','world'];
let strArr2: Array<string> = ['Hello', 'world'];

let anyArr: any[] = ['Hello', 10, true];

let myTuple: [string, number] = ["Hi", 10];
console.log(myTuple[0]);
console.log(myTuple[1]);
*/
/* Sample 3: Tuples
        We can add elements as long as they are of the same type specified by declaring this Array.
    Result:
        Hi
        10
        100
*/
var strArr1 = ['Hello', 'world'];
var strArr2 = ['Hello', 'world'];
var anyArr = ['Hello', 10, true];
var myTuple = ["Hi", 10];
console.log(myTuple[0]);
console.log(myTuple[1]);
myTuple[2] = 100;
console.log(myTuple[2]);
//# sourceMappingURL=tutorial46.js.map