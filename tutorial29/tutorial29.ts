/*
   Sets & Maps
*/


/* Sample 1: Set
   Output:
        id exists
*/
/* let mySet = Object.create(null);
mySet.id = true;
if(mySet.id){
    console.log("id exists");

} */


/* Sample 2: Set
   Output:
        id exists
*/
/* let mySet = Object.create(null);
mySet.id = 1;
if(mySet.id){
    console.log("id exists");

} */


/* Sample 3: Set
   Output:
        EMPTY
*/
/* let mySet = Object.create(null);
mySet.id = 0;
if(mySet.id){
    console.log("id exists");

} */


/* Sample 4: Map
   Output:
        Chandler
        Hello
        World
        [object Object]
        [object Object]
*/
let mySet = Object.create(null);
mySet.id = 0;
if(mySet.id){
    console.log("id exists");

}

let myMap = Object.create(null);
myMap.name = "Chandler";
let val = myMap.name;
console.log(val);

myMap[100] = "Hello";
console.log(myMap["100"]);


//Keyword can be object.
let obj1 = {};
let obj2 = {};

myMap[obj1] = "World";

//console.log(myMap[obj1]);
console.log(myMap[obj2]);

console.log(obj1.toString());
console.log(obj2.toString());


