"use strict";
/*
   Sets
*/
/* Sample 1
   Output:
        1
*/
/* let mySet = new Set();

mySet.add("Hello");
console.log(mySet.size); */
/* Sample 2
   Output:
        2
*/
/* let mySet = new Set();

mySet.add("Hello");
mySet.add(1);
console.log(mySet.size); */
/* Sample 3
   Output:
        2
*/
/* let mySet = new Set();

mySet.add("Hello");
mySet.add(1);
mySet.add(1);
console.log(mySet.size); */
/* Sample 4
   Output:
        4
*/
/* let mySet = new Set();
let obj1 = {};
let obj2 = {};

mySet.add("Hello");
mySet.add(1);
mySet.add(obj1);
mySet.add(obj2);
console.log(mySet.size); */
/* Sample 5
   Output:
        4
        4
*/
/* let mySet = new Set();
let obj1 = {};
let obj2 = {};

mySet.add("Hello");
mySet.add(1);
mySet.add(obj1);
mySet.add(obj2);
console.log(mySet.size);

let newSet = new Set([1,2,3,4,4,4]);
console.log(newSet.size); */
/* Sample 6: Chain set
   Output:
        4
        4
        2
*/
/* let mySet = new Set();
let obj1 = {};
let obj2 = {};

mySet.add("Hello");
mySet.add(1);
mySet.add(obj1);
mySet.add(obj2);
console.log(mySet.size);

let newSet = new Set([1,2,3,4,4,4]);
console.log(newSet.size);

let chainSet = new Set().add("hello").add("world");
console.log(chainSet.size); */
/* Sample 7: To check for existence of a value in a set, use has methhod.
   Output:
        4
        4
        2
        //true
        false
*/
/* let mySet = new Set();
let obj1 = {};
let obj2 = {};

mySet.add("Hello");
mySet.add(1);
mySet.add(obj1);
mySet.add(obj2);
console.log(mySet.size);

let newSet = new Set([1,2,3,4,4,4]);
console.log(newSet.size);

let chainSet = new Set().add("hello").add("world");
console.log(chainSet.size);

//console.log(newSet.has(1));
console.log(newSet.has(5)); */
/* Sample 8: To delete a value from a set, use delete methhod.
   Output:
        4
        4
        2
        true
        3
*/
var mySet = new Set();
var obj1 = {};
var obj2 = {};
mySet.add("Hello");
mySet.add(1);
mySet.add(obj1);
mySet.add(obj2);
console.log(mySet.size);
var newSet = new Set([1, 2, 3, 4, 4, 4]);
console.log(newSet.size);
var chainSet = new Set().add("hello").add("world");
console.log(chainSet.size);
console.log(newSet.delete(1));
console.log(newSet.size);
//# sourceMappingURL=tutorial30.js.map