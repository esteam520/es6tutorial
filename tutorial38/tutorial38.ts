/*
   Iterables and Iterators
*/


/*
   Interpretation
*/
/*
Iterable {
    [symbol.Iterator]() : Iterator
}

Iterator{
    next() : IResultOjb
}
IResultObj{
    value: any
    done: bool
}
*/


/* Sample
   Output:
        {value: 1, done: false}
        {value: 2, done: false}
        {value: 3, done: false}
        {value: undefined, done: true}
   Output (Tutorial)
        Object {value: 1, done: false}
        Object {value: 2, done: false}
        Object {value: 3, done: false}
        Object {value: undefined, done: true}
   Note:
        Why the output is different?
*/
let iterable = [1,2,3];

function createIterator(array){
    let count = 0;
    return{
        next: function(){
            return count < array.length?
            {value: array[count++], done:false}:
            {value: undefined, done:true};
        }
    }
}

let myIterator = createIterator(iterable);

console.log(myIterator.next());
console.log(myIterator.next());
console.log(myIterator.next());
console.log(myIterator.next());


