"use strict";
var getRegvalue = function () {
    return 10;
};
console.log(getRegvalue());
// Samples for Arrow function
// Sample 1
/* const getArrowvalue = () => {
    return 10;
}
console.log(getArrowvalue()); */
// Sample 2
//const getArrowvalue = (m) => 10*m
//console.log(getArrowvalue(5));
// Sample 3
//const getArrowvalue = m => 10*m
//console.log(getArrowvalue(5));
// Sample 4
var getArrowvalue = function (m, bonus) { return 10 * m + bonus; };
console.log(getArrowvalue(5, 50));
console.log(typeof getArrowvalue);
//# sourceMappingURL=tutorial10.js.map