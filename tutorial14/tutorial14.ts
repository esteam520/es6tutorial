/* Sample 1
   Output:
        List of colors
        ["Orange", "Yellow", "Indigo"]
        Orange
        Yellow
        Indigo
*/
/* let displayColors = function(message, ...colors){

    console.log(message);
    console.log(colors);


    for(let i in colors){
        console.log(colors[i]);

    }
}

let message = "List of colors"

let colorArray = ['Orange','Yellow','Indigo'];
displayColors(message, ...colorArray) */

//displayColors(message, 'Red');
//displayColors(message, 'Red','Blue');
//displayColors(message, 'Red','Blue','Green');


/* Sample 2
   Output:
        List of colors
        Orange
        Yellow
        Indigo
*/
let displayColors = function(message, ...colors){

    console.log(message);
//    console.log(colors);


    for(let i in colors){
        console.log(colors[i]);

    }
}

let message = "List of colors"

let colorArray = ['Orange','Yellow','Indigo'];
displayColors(message, ...colorArray)

//displayColors(message, 'Red');
//displayColors(message, 'Red','Blue');
//displayColors(message, 'Red','Blue','Green');

