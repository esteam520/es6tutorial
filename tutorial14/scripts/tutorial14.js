"use strict";
/* Sample 1
   Output:
        List of colors
        ["Orange", "Yellow", "Indigo"]
        Orange
        Yellow
        Indigo
*/
/* let displayColors = function(message, ...colors){

    console.log(message);
    console.log(colors);


    for(let i in colors){
        console.log(colors[i]);

    }
}

let message = "List of colors"

let colorArray = ['Orange','Yellow','Indigo'];
displayColors(message, ...colorArray) */
//displayColors(message, 'Red');
//displayColors(message, 'Red','Blue');
//displayColors(message, 'Red','Blue','Green');
/* Sample 2
   Output:
        List of colors
        Orange
        Yellow
        Indigo
*/
var displayColors = function (message) {
    var colors = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        colors[_i - 1] = arguments[_i];
    }
    console.log(message);
    //    console.log(colors);
    for (var i in colors) {
        console.log(colors[i]);
    }
};
var message = "List of colors";
var colorArray = ['Orange', 'Yellow', 'Indigo'];
displayColors.apply(void 0, [message].concat(colorArray));
//displayColors(message, 'Red');
//displayColors(message, 'Red','Blue');
//displayColors(message, 'Red','Blue','Green');
//# sourceMappingURL=tutorial14.js.map