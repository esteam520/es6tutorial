"use strict";
/*
   Class Constructors
*/
/* Sample
*/
var Person = /** @class */ (function () {
    function Person(fname, lname) {
        this.fname = fname;
        this.lname = fname;
    }
    return Person;
}());
;
// Angular 2 Syntax
var Person1 = /** @class */ (function () {
    function Person1(fname, lname) {
        this.fname = fname;
        this.lname = lname;
    }
    return Person1;
}());
//# sourceMappingURL=tutorial47.js.map