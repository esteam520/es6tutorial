/*
   Class Constructors
*/


/* Sample
*/
class Person{
    public fname: string;
    public lname: string;
    constructor(fname: string, lname: string) {
        this.fname = fname;
        this.lname = fname;
    }
};

// Angular 2 Syntax
class Person1{
    constructor(public fname: string, public lname: string){

    }
}