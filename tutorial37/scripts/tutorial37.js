"use strict";
/*
   Symbol iterator: for..of -- iterator method --> symbol.iterator.
*/
/* Sample: Checking to see if a method exists for the object and this particular key.
           If it returns a method or a function, then this object can be used with the for..of loop.
   Output:
        For string -function
        For array -function
        For number -undefined
        For object -undefined
   Note:
        object can't use for..of loop. it doesn't mean that objects are not retrievable.
        We can write a own special iterator method. So that even object can be used with for..of loop.
*/
var str = "Hello";
var arr = [1, 2, 3];
var num = 5;
var obj = { name: "Chandler" };
console.log("For string -" + typeof str[Symbol.iterator]);
console.log("For array -" + typeof arr[Symbol.iterator]);
console.log("For number -" + typeof num[Symbol.iterator]);
console.log("For object -" + typeof obj[Symbol.iterator]);
//# sourceMappingURL=tutorial37.js.map