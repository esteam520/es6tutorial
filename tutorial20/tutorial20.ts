/*
   for of Loop
*/

/* Sample: Normal for Loop vs for of Loop
   Output:
        Red
        Blue
        Green
        Red
        Blue
        Green
        A
        B
        C
*/
let colors = ['Red', 'Blue', 'Green'];

for(let index in colors){
    console.log(colors[index]);

}

for(let color of colors){
    console.log(color);
}

let letters = "ABC";

for(let letter of letters){
    console.log(letter);
}
