"use strict";
/* Sample 1
   Output:
        Chandler
        Bing
        Female
*/
//let employee = ["Chandler", "Bing", "Female"];
/* let employee = {
        fname: "Chandler",
        lname: "Bing",
        gender: "Female"
}

let {fname, lname, gender} = employee;

//let [fname, lname, gender="Male"] = employee;

console.log(fname);
console.log(lname);
console.log(gender); */
/* Sample 2: Use Alias
   Output:
        Chandler
        Bing
        Female
*/
//let employee = ["Chandler", "Bing", "Female"];
var employee = {
    fname: "Chandler",
    lname: "Bing",
    gender: "Female"
};
var f = employee.fname, l = employee.lname, g = employee.gender;
//let [fname, lname, gender="Male"] = employee;
console.log(f);
console.log(l);
console.log(g);
//# sourceMappingURL=tutorial18.js.map